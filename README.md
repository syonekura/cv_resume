# CV Resume

![coverage](https://www.gitlab.com/syonekura/cv_resume/badges/main/coverage.svg?job=pytest)
![pipeline status](https://www.gitlab.com/syonekura/cv_resume/badges/main/pipeline.svg)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Personal project to maintain my CV.

## Where?
[Click here](https://gitlab.com/syonekura/cv_resume/-/jobs/artifacts/main/browse?job=generate_pdf) to browse the latest version of my curriculum


## Pre-requisites

This projects runs with:

- [pyenv](https://github.com/pyenv/pyenv#installation)
- Python 3.11. Use `pyenv` to install it.
- [Poetry](https://python-poetry.org/docs/#installation)

## Setup

After cloning the repo, configure the repo directory to use Python-3.11 (`pyenv local 3.11...`), then execute `poetry install`.

## Command cheatsheet

Generate a `.tex` CV by running

```bash
poetry run cv <outputFile>
```

Then get the pdf output by executing

```bash
pdflatex <outputFile>
```
