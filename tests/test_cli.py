import os.path

from click.testing import CliRunner

from cv_resume.cli import generate_tex


def test_cli():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(generate_tex, ["output.tex"])
        assert result.exit_code == 0
        assert (
            "Generation of ModernCV with style=CLASSIC and color=GREEN started"
            in result.output
        )
        assert os.path.exists("output.tex")
