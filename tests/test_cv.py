from cv_resume.cv import cv_data


def test_cv_data(snapshot):
    data = cv_data()
    snapshot.snapshot_dir = "snapshots"
    snapshot.assert_match(data.json(indent=2), "cv_data.json")
