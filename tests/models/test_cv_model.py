from datetime import datetime
from typing import List

import pytest
from pytest_mock import MockerFixture

from cv_resume.models.commons import TimeRange
from cv_resume.models.cv_model import Color, CVModel, ModernCV, Style
from cv_resume.models.education import EducationEntry
from cv_resume.models.personal_data import PersonalData
from cv_resume.models.projects import CompanyProjects, ProjectEntry
from cv_resume.models.work_exp import WorkExperienceEntry


@pytest.mark.parametrize(
    "social, filename, education",
    [
        ({"instagram": "jdoe", "linkedin": "jdoe"}, "jdoe_social.tex", []),
        (
            {},
            "jdoe_none_social.tex",
            [
                EducationEntry(
                    years=TimeRange(
                        start=datetime(2023, 1, 1), end=datetime(2023, 5, 1)
                    ),
                    degree="TestDegree",
                    institution="Test Institution",
                    city="TestCity",
                )
            ],
        ),
    ],
)
def test_moderncv(snapshot, social, filename, education):
    personal_data = PersonalData(
        name=("John", "Doe"),
        phone="+1 123 456 7890",
        email="john@doe.com",
        social=social,
        photo="testPhoto.png",
    )

    cv = ModernCV(
        personal_data=personal_data,
        style=Style.FANCY,
        color=Color.RED,
    )
    cv.make_title()
    cv.add_education(education)

    snapshot.snapshot_dir = "snapshots"
    snapshot.assert_match(cv.dumps(), filename)


def test_add_work_exp_no_entry(mocker: MockerFixture):
    mocked_modern_cv = mocker.MagicMock()

    entries: List[WorkExperienceEntry] = []
    ModernCV.add_work_exp(mocked_modern_cv, entries)
    mocked_modern_cv.create.assert_not_called()


def test_add_work_exp_entries(mocker: MockerFixture):
    mocked_modern_cv = mocker.MagicMock()
    mocked_work_exp_cv = mocker.patch(
        "cv_resume.models.cv_model.WorkExperienceCV"
    )
    entries = [1, 2, 3]

    # noinspection PyTypeChecker
    ModernCV.add_work_exp(mocked_modern_cv, entries=entries)  # type: ignore
    mocked_work_exp_cv.assert_has_calls(
        [
            mocker.call(work_exp=1),
            mocker.call(work_exp=2),
            mocker.call(work_exp=3),
        ]
    )
    mocked_modern_cv.create.assert_called()
    mocked_modern_cv.append.assert_called()


def test_add_projects_no_entry(mocker: MockerFixture):
    mocked_modern_cv = mocker.MagicMock()
    entries: List[CompanyProjects] = []
    ModernCV.add_projects(mocked_modern_cv, entries)
    mocked_modern_cv.create.assert_not_called()


def test_add_projects_entries(mocker: MockerFixture):
    mocked_modern_cv = mocker.MagicMock()
    mocked_work_exp_cv = mocker.patch(
        "cv_resume.models.cv_model.ProjectEntryCV"
    )
    entries = [
        CompanyProjects(
            company_name="test1",
            projects=[
                ProjectEntry(
                    name="test1Name",
                    description="test1Description",
                    technologies=[],
                )
            ],
        )
    ]

    # noinspection PyTypeChecker
    ModernCV.add_projects(mocked_modern_cv, entries=entries)  # type: ignore
    mocked_work_exp_cv.assert_has_calls(
        [
            mocker.call(
                project_data=ProjectEntry(
                    name="test1Name",
                    description="test1Description",
                    technologies=[],
                )
            )
        ]
    )
    mocked_modern_cv.create.assert_called()
    mocked_modern_cv.append.assert_called()


def test_cv_model_to_modern_cv(mocker: MockerFixture):
    modern_cv_mock = mocker.patch(
        "cv_resume.models.cv_model.ModernCV"
    ).return_value

    base_model = CVModel(
        personal_data=PersonalData(
            name=("testName", "testLastName"),
            phone="+1 234 567 8900",
            email="something@somewhere.com",
            photo="testPhoto.png",
        )
    )
    result = base_model.to_modern_cv(style=Style.FANCY, color=Color.RED)

    assert result == modern_cv_mock
    modern_cv_mock.make_title.assert_called_once()
    modern_cv_mock.add_work_exp.assert_called_once()
    modern_cv_mock.add_education.assert_called_once()
    modern_cv_mock.add_projects.assert_called_once()
