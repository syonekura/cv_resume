from datetime import datetime

import pytest

from cv_resume.models.commons import TimeRange
from cv_resume.models.work_exp import WorkExperienceCV, WorkExperienceEntry


@pytest.mark.parametrize(
    "data",
    [
        WorkExperienceEntry(
            years=TimeRange(
                start=datetime(2023, 1, 1), end=datetime(2023, 5, 1)
            ),
            job_title="Rocket scientist",
            company="Space X",
            address="The Moon",
            short_description="Lead engineer in charge of bike shedding all projects",
        ),
        WorkExperienceEntry(
            years=TimeRange(
                start=datetime(2023, 1, 1), end=datetime(2023, 5, 1)
            ),
            job_title="multiline Rocket scientist",
            company="Space X",
            address="The Moon",
            short_description="""Lead engineer
            in charge of bike shedding
            all projects""",
        ),
    ],
)
def test_projects_entry_cv(data: WorkExperienceEntry, snapshot):
    cv = WorkExperienceCV(work_exp=data)
    snapshot.snapshot_dir = "snapshots"
    snapshot.assert_match(cv.dumps(), data.job_title + ".tex")
