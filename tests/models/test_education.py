from datetime import datetime

import pytest

from cv_resume.models.commons import TimeRange
from cv_resume.models.education import EducationEntry, EducationEntryCV


@pytest.mark.parametrize(
    "entry",
    [
        EducationEntry(
            years=TimeRange(
                start=datetime(2020, 1, 1), end=datetime(2023, 1, 1)
            ),
            degree="Ball pen engineering",
            institution="Aplaplac",
            city="Santiago",
        ),
        EducationEntry(
            years=TimeRange(
                start=datetime(2020, 1, 1), end=datetime(2023, 1, 1)
            ),
            degree="Gastronomy with Masters in toasted bread and ice",
            institution="Aplaplac",
            city="Santiago",
            thesis="Test Thesis",
        ),
        EducationEntry(
            years=TimeRange(
                start=datetime(2020, 1, 1), end=datetime(2023, 1, 1)
            ),
            degree="Journalistic journalism",
            institution="Aplaplac",
            city="Santiago",
            thesis="Test Thesis",
            comment="TestComment",
        ),
    ],
)
def test_education_entry_cv(entry: EducationEntry, snapshot):
    cv = EducationEntryCV(ed_entry=entry)
    snapshot.snapshot_dir = "snapshots"
    snapshot.assert_match(cv.dumps(), entry.degree.replace(" ", "_") + ".tex")
