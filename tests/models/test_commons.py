from contextlib import nullcontext as does_not_raise
from datetime import datetime

import pytest

from cv_resume.models.commons import TimeRange


@pytest.mark.parametrize(
    "start, end, context, str_fmt",
    [
        (
            datetime(2023, 1, 1),
            datetime(2023, 2, 1),
            does_not_raise(),
            "2023-01--2023-02",
        ),
        (
            datetime(2023, 2, 1),
            datetime(2023, 1, 1),
            pytest.raises(ValueError),
            None,
        ),
        (
            datetime(2023, 1, 1),
            "Present",
            does_not_raise(),
            "2023-01--Present",
        ),
    ],
)
def test_time_range(start, end, context, str_fmt):
    with context:
        assert str(TimeRange(start=start, end=end)) == str_fmt
