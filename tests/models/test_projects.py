import pytest

from cv_resume.models.projects import ProjectEntry, ProjectEntryCV


@pytest.mark.parametrize(
    "data",
    [
        (
            ProjectEntry(
                name="TestName",
                description="Test Description",
                technologies=["tec1", "tec2"],
            )
        )
    ],
)
def test_projects_entry_cv(data: ProjectEntry, snapshot):
    cv = ProjectEntryCV(project_data=data)
    snapshot.snapshot_dir = "snapshots"
    snapshot.assert_match(cv.dumps(), data.name + ".tex")
