import click

from cv_resume.cv import cv_data
from cv_resume.models.cv_model import Color, Style


@click.command()
@click.argument("output")
@click.option(
    "--style",
    default=Style.CLASSIC,
    type=click.Choice(Style),
    help="Sets the ModernCV style",
)
@click.option(
    "--color",
    default=Color.GREEN,
    type=click.Choice(Color),
    help="Sets the ModernCV color",
)
def generate_tex(output: str, style: Style, color: Color):
    click.echo(f"Generation of ModernCV with {style=} and {color=} started")
    data = cv_data()
    modern_cv = data.to_modern_cv(style, color)
    with open(output, "w") as f:
        modern_cv.dump(f)
    click.echo("Done")
