from typing import Optional

from pydantic import BaseModel
from pylatex.base_classes import CommandBase
from pylatex.utils import NoEscape, italic

from cv_resume.models.commons import TimeRange


class EducationEntry(BaseModel):
    years: TimeRange
    degree: str
    institution: str
    city: str
    thesis: Optional[str]
    comment: Optional[str]


class EducationEntryCV(CommandBase):
    _latex_name = "cventry"

    def __init__(self, ed_entry: EducationEntry):
        arguments = [
            NoEscape(str(ed_entry.years)),
            ed_entry.degree,
            ed_entry.institution,
            ed_entry.city,
        ]

        if ed_entry.thesis:
            arguments.append(italic(ed_entry.thesis))
        else:
            arguments.append("")

        if ed_entry.comment:
            arguments.append(ed_entry.comment)
        else:
            arguments.append("")

        super().__init__(arguments=arguments)
