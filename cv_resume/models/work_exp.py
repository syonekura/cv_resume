from pydantic import BaseModel
from pylatex import NoEscape
from pylatex.base_classes import CommandBase

from cv_resume.models.commons import TimeRange


class WorkExperienceEntry(BaseModel):
    years: TimeRange
    job_title: str
    company: str
    address: str
    short_description: str


class WorkExperienceCV(CommandBase):
    _latex_name = "cventry"

    def __init__(self, work_exp: WorkExperienceEntry):
        arguments = [
            NoEscape(str(work_exp.years)),
            NoEscape(work_exp.job_title),
            NoEscape(work_exp.company),
            NoEscape(work_exp.address),
            "",
            NoEscape(work_exp.short_description),
        ]
        super().__init__(arguments=arguments)
