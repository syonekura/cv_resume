from typing import Dict, Tuple

from pydantic import BaseModel

SocialData = Dict[str, str]


class PersonalData(BaseModel):
    name: Tuple[str, str]
    phone: str
    email: str
    social: SocialData = dict()
    photo: str
