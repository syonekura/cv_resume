from typing import List

from pydantic import BaseModel
from pylatex.base_classes import CommandBase
from pylatex.utils import NoEscape, bold


class ProjectEntry(BaseModel):
    name: str
    description: str
    technologies: List[str]


class ProjectEntryCV(CommandBase):
    _latex_name = "cvitem"

    def __init__(self, project_data: ProjectEntry):
        arguments = [
            bold(project_data.name),
            NoEscape(
                project_data.description
                + NoEscape(r"\break")
                + bold("Technologies: ", escape=False)
                + ", ".join(project_data.technologies)
            ),
        ]
        super().__init__(arguments=arguments)


class CompanyProjects(BaseModel):
    company_name: str
    projects: List[ProjectEntry]
