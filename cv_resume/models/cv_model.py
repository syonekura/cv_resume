from enum import StrEnum
from typing import List

from pydantic import BaseModel
from pylatex import Command, Document, NoEscape, Package, Section, Subsection

from cv_resume.models.education import EducationEntry, EducationEntryCV
from cv_resume.models.personal_data import PersonalData
from cv_resume.models.projects import CompanyProjects, ProjectEntryCV
from cv_resume.models.work_exp import WorkExperienceCV, WorkExperienceEntry


class Style(StrEnum):
    """
    Modern CV Styles
    """

    CASUAL = "casual"
    CLASSIC = "classic"
    BANKING = "banking"
    OLDSTYLE = "oldstyle"
    FANCY = "fancy"

    def __repr__(self):
        return self.value.upper()


class Color(StrEnum):
    """
    Modern CV Colors
    """

    BLACK = "black"
    BLUE = "blue"
    BURGUNDY = "burgundy"
    GREEN = "green"
    GREY = "grey"
    ORANGE = "orange"
    PURPLE = "purple"
    RED = "red"

    def __repr__(self):
        return self.value.upper()


class ModernCV(Document):
    def __init__(
        self,
        personal_data: PersonalData,
        style: Style = Style.CASUAL,
        color: Color = Color.BLUE,
    ):
        super().__init__(
            documentclass="moderncv",
            document_options=["12pt", "letterpaper", "sans"],
        )
        # Empty packages
        self.packages: List[Package] = []

        self.preamble += [
            Command("moderncvstyle", style.value),
            Command("moderncvcolor", color.value),
            Command("usepackage", "geometry", options="scale=0.75"),
            Command(
                "setlength", arguments=[Command("footskip"), "149.60005pt"]
            ),
            # Font loading
            Command("ifxetexorluatex"),
            Command("usepackage", "fontspec"),
            Command("usepackage", NoEscape("unicode-math")),
            Command("defaultfontfeatures", "Ligatures=TeX"),
            Command("setmainfont", "Latin Modern Roman"),
            Command("setmainfont", "Latin Modern Sans"),
            Command("setmainfont", "Latin Modern Mono"),
            Command("setmainfont", "Latin Modern Math"),
            Command("else"),
            Command("usepackage", "inputenc", options="utf8"),
            Command("usepackage", "fontenc", options="T1"),
            Command("usepackage", "lmodern"),
            Command("fi"),
            Command("usepackage", "babel", options="english"),
            # Adjust width of hint column based on date range format
            NoEscape(r"\settowidth{\hintscolumnwidth}{YYYY-MM--YYYY-MM}"),
        ]

        # Personal Data
        self.preamble += [
            Command("name", arguments=list(personal_data.name)),
            Command("phone", personal_data.phone, options="mobile"),
            Command("email", personal_data.email),
        ]

        # Social
        self.preamble += [
            Command("social", username, options=platform)
            for platform, username in personal_data.social.items()
        ]
        # Photo
        self.preamble.append(
            Command(
                "photo",
                options=["64pt"],
                arguments=[f"img/{personal_data.photo}"],
            )
        )

    def make_title(self):
        self.append(Command("makecvtitle"))

    def add_education(self, entries: List[EducationEntry]):
        if not entries:
            return
        with self.create(Section("Education")):
            for entry in entries:
                self.append(EducationEntryCV(entry))

    def add_work_exp(self, entries: List[WorkExperienceEntry]):
        if not entries:
            return
        with self.create(Section("Work Experience")):
            for entry in entries:
                self.append(WorkExperienceCV(work_exp=entry))

    def add_projects(self, entries: List[CompanyProjects]):
        if not entries:
            return

        with self.create(Section("Projects")):
            for company_projects in entries:
                with self.create(Subsection(company_projects.company_name)):
                    for project in company_projects.projects:
                        self.append(ProjectEntryCV(project_data=project))


class CVModel(BaseModel):
    personal_data: PersonalData
    education: List[EducationEntry] = []
    work_experience: List[WorkExperienceEntry] = []
    projects: List[CompanyProjects] = []

    def to_modern_cv(self, style: Style, color: Color) -> ModernCV:
        curriculum = ModernCV(
            personal_data=self.personal_data, style=style, color=color
        )
        curriculum.make_title()
        curriculum.add_work_exp(entries=self.work_experience)
        curriculum.add_education(entries=self.education)
        curriculum.add_projects(entries=self.projects)
        return curriculum
