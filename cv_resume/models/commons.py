from datetime import datetime
from typing import Literal

import pydantic
from pydantic import BaseModel


class TimeRange(BaseModel):
    start: datetime
    end: datetime | Literal["Present"]

    @pydantic.validator("end")
    def _val(cls, value, values):
        if value != "Present" and value < values["start"]:
            raise ValueError(
                f"End date ({value}) defined before start date ({values['start']})"
            )
        return value

    def __str__(self):
        fmt = "%Y-%m"
        if self.end != "Present":
            return f"{self.start.strftime(fmt)}--{self.end.strftime(fmt)}"
        else:
            return f"{self.start.strftime(fmt)}--{self.end}"
