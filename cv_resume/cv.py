from datetime import datetime

from pylatex import NoEscape, escape_latex

from cv_resume.models.commons import TimeRange
from cv_resume.models.cv_model import CVModel
from cv_resume.models.education import EducationEntry
from cv_resume.models.personal_data import PersonalData
from cv_resume.models.projects import CompanyProjects, ProjectEntry
from cv_resume.models.work_exp import WorkExperienceEntry


def hyperlink(url, text):
    text = escape_latex(text)
    return NoEscape(r"\href{" + url + r"}{\color{blue}" + text + "}")


def cv_data() -> CVModel:
    personal_data = PersonalData(
        name=("Sebastian", "Yonekura Baeza"),
        phone="+1 571 400 9383",
        email="sebastian.yonekura@gmail.com",
        social={
            "linkedin": "syonekura",
            "github": "syonekura",
            "gitlab": "syonekura",
        },
        photo="photo.png",
    )
    education = [
        EducationEntry(
            years=TimeRange(
                start=datetime(2008, 1, 1), end=datetime(2014, 12, 1)
            ),
            degree="Electrical Engineer",
            institution="Universidad de Chile",
            city=hyperlink(
                "https://goo.gl/maps/Sfdhc2JcQmW5rMnY8",
                "Av. Beauchef 850, Santiago, Region Metropolitana, Chile",
            ),
            thesis="Thesis: Evaluation and improvement of a iris recognition system "
            "at a distance using a high resolution camera",
            comment="Degree obtained with maximum distinction",
        ),
        EducationEntry(
            years=TimeRange(
                start=datetime(2003, 1, 1), end=datetime(2006, 1, 1)
            ),
            degree="High school",
            institution="Intituto Nacional Jose Miguel Carrera",
            city=hyperlink(
                "https://goo.gl/maps/HDqyyxkyaE5aybrE6",
                "Arturo Prat 33, Santiago, Region Metropolitana, Chile",
            ),
        ),
    ]
    work_exp = [
        WorkExperienceEntry(
            years=TimeRange(start=datetime(2021, 1, 11), end="Present"),
            address=hyperlink(
                "https://goo.gl/maps/DfzSiZC8W2KzLy4m7",
                "Amazon HQ2, 2100 Crystal Drive, Arlington, Virginia, US",
            ),
            company="Amazon",
            job_title="Software Developer Engineer",
            short_description="""Design and implementation of a rules engine
            system for Amazon Finance and Global Business Services (FGBS).""",
        ),
        WorkExperienceEntry(
            years=TimeRange(
                start=datetime(2017, 11, 1), end=datetime(2020, 12, 1)
            ),
            company="Cencosud S.A.",
            address=hyperlink(
                "https://goo.gl/maps/1M3MphAG2URmvu4B6",
                "Av. Kennedy 9001, Las Condes, Santiago, Chile",
            ),
            job_title="Senior Data Scientist / Software Developer",
            short_description="""Design and implementation of several machine learning
            based services within Cencosud's Advanced Analytics
            Department""",
        ),
        WorkExperienceEntry(
            years=TimeRange(
                start=datetime(2015, 5, 1), end=datetime(2017, 10, 1)
            ),
            address=hyperlink(
                "https://goo.gl/maps/7g1QrsRJokYXWGqH7",
                "San Sebastian 2952, Las Condes, Santiago, Chile",
            ),
            job_title="Software Engineer",
            company="MapCity S.A.",
            short_description="""Main developer of all backend
            services requiring analytics, Full Text Search and NLP""",
        ),
        WorkExperienceEntry(
            years=TimeRange(
                start=datetime(2012, 12, 1), end=datetime(2013, 12, 1)
            ),
            company="WoodTech Measurement Solutions S.A.",
            address=hyperlink(
                "https://goo.gl/maps/oUBTT24Xa3Re36bS6",
                "Av. El Bosque Norte 5, Las Condes, Santiago, Chile",
            ),
            job_title="Software Engineer",
            short_description="""Second internship and work as outsource:
            Design and implementation of detection algorithms and filters
            over point clouds obtained from laser scanners in 3D for
            volumetric estimation of raw materials transported over trucks.""",
        ),
        WorkExperienceEntry(
            years=TimeRange(
                start=datetime(2012, 2, 1), end=datetime(2012, 5, 1)
            ),
            company=r"Foris Integraci\'on y Consultor\'ia",
            address=hyperlink(
                "https://goo.gl/maps/CNLMYvUz5BhjdynB9",
                "El Comendador 1983, Providencia, Santiago, Chile",
            ),
            job_title="Software Engineer",
            short_description="""
            First internship: Design and implementation of genetic algorithms applied
            to the Timetabling problem in latino-american higher education
            institutions.
            """,
        ),
    ]
    projects = [
        CompanyProjects(
            company_name="Amazon",
            projects=[
                ProjectEntry(
                    name="DC3 Rules Engine Executor",
                    description=r"""
                    I implemented a Rules execution engine for Finance and Accounting
                    teams to host and execute custom business logic. The application
                    is used to determine correct asset categories for purchase orders
                    within te company based on a set of custom rules defined by Finance
                    and Accounting teams, reducing several hours of manual labor and
                    moving the company one step forward to deprecate Oracle Finance
                    products.
                    \break
                    The application is hosted on AWS Cloud with API Gateway backed by
                    AWS Lambda functions written in Python and TypeScript, following a
                    serverless architecture, with full CI/CD pipelines. We also vended
                    a Java Client for our internal tech customers to be able to easily
                    consume that logic.
                    """,
                    technologies=[
                        hyperlink("https://github.com/aws/aws-cdk", "AWS CDK"),
                        hyperlink(
                            "https://www.typescriptlang.org/", "TypeScript"
                        ),
                        hyperlink(
                            "https://docs.oracle.com/en/java/javase/11/",
                            "Java 11",
                        ),
                        hyperlink(
                            "https://docs.python.org/3.9/", "Python 3.9"
                        ),
                        hyperlink("https://fastapi.tiangolo.com/", "FastAPI"),
                    ],
                ),
                ProjectEntry(
                    name="DC3 Rules Management Service",
                    description=r"""
                    I designed and implemented a rule management service that allows
                    our customers to define, update, test and deploy rules in a self
                    service manner. Incorporates an approval workflow that let our
                    business users to review and test changes before exposing them to
                    production traffic.
                    \break
                    The backend application is hosted on AWS Cloud with API Gateway
                    backed by Lambda, Step Functions, SQS, S3 and Dynamo DB. All
                    written in Python and vending clients for Java, Python and
                    TypeScript/JavaScript. The frontend UI is a React based website
                    hosted on AWS CloudFront.
                    """,
                    technologies=[
                        hyperlink("https://github.com/aws/aws-cdk", "AWS CDK"),
                        hyperlink(
                            "https://www.typescriptlang.org/", "TypeScript"
                        ),
                        hyperlink(
                            "https://docs.python.org/3.9/", "Python 3.9"
                        ),
                        hyperlink("https://www.cypress.io/", "Cypress"),
                        hyperlink("https://react.dev/", "React"),
                        hyperlink("https://www.openapis.org/", "OpenAPI"),
                    ],
                ),
            ],
        ),
        CompanyProjects(
            company_name="Cencosud S.A.",
            projects=[
                ProjectEntry(
                    name="Messi",
                    description="""
                    Developed sales forecasts and optimization suggestions for
                    hardware stores in Chile (Easy stores) with the objective
                    to reduce warehouse space. These forecasts were produced
                    on a monthly cadence as an Airflow job running on Kubernetes.
                    """,
                    technologies=[
                        hyperlink(
                            "https://azure.microsoft.com/en-us",
                            "Microsoft Azure",
                        ),
                        hyperlink("https://kubernetes.io/", "Kubernetes"),
                        hyperlink("https://www.docker.com/", "Docker"),
                        hyperlink(
                            "https://docs.python.org/3.7/", "Python 3.7"
                        ),
                        hyperlink("https://airflow.apache.org/", "Airflow"),
                    ],
                ),
                ProjectEntry(
                    name="Flex",
                    description="""
                    Design and implementation of an optimization model to find the
                    best assignments for store worker's shifts, taking into account
                    several features like demand forecast, labour / legal constraints
                    and each worker availability.
                    """,
                    technologies=[
                        hyperlink(
                            "https://azure.microsoft.com/en-us",
                            "Microsoft Azure",
                        ),
                        hyperlink("https://kubernetes.io/", "Kubernetes"),
                        hyperlink("https://www.docker.com/", "Docker"),
                        hyperlink(
                            "https://docs.python.org/3.7/", "Python 3.7"
                        ),
                    ],
                ),
                ProjectEntry(
                    name="People Counter",
                    description="""
                    Developed and deployed an experimental people counter to check
                    for number of customers within the stores using Deep Learning
                    models. These counters were added into already existent security
                    camera networks and generated real time streams for every
                    customer within the stores. Features of each customer were
                    extracted to recognize them and avoid skewing the counts. On a
                    second stage of a tracker was added to the computer vision
                    pipeline, this allowed to get metrics about space usage and
                    determine hotspots within the stores.
                    """,
                    technologies=[
                        hyperlink(
                            "https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/",
                            "Nvidia Jetson",
                        ),
                        hyperlink(
                            "https://gstreamer.freedesktop.org/", "GStreamer"
                        ),
                        hyperlink(
                            "https://developer.nvidia.com/deepstream-sdk",
                            "Nvidia Deepstream",
                        ),
                        hyperlink("https://isocpp.org/", "C++"),
                        hyperlink(
                            "https://azure.microsoft.com/en-us/solutions/iot",
                            "Azure IoT",
                        ),
                    ],
                ),
            ],
        ),
        CompanyProjects(
            company_name="MapCity S.A.",
            projects=[
                ProjectEntry(
                    name="Geolocalizador",
                    description="""
                    On this project I inherited and improved an endpoint service that
                    expects a string with a chilean address and returns
                    geo-localization coordinates, as well as zip code and related
                    metadata. Several improvements were made, among them: reducing RAM
                    footprint by more than half; reducing p99 latency by 1 order of
                    magnitude; improving the NLP/Full Text Search algorithms used
                    behind the scenes; adding tests and a CI/CD pipeline. In the span
                    of a year the company went from having an unreliable service
                    deployed on three clusters with 3 customers onboarded, to having
                    8 customers onboarded on a service that required a third of the
                    infrastructure deployed at the beginning.
                    """,
                    technologies=[
                        hyperlink(
                            "https://docs.oracle.com/javase/8/docs/api/",
                            "Java 8",
                        ),
                        hyperlink("https://spring.io/", "Spring Framework"),
                        hyperlink(
                            "https://lucene.apache.org/", "Apache Lucene"
                        ),
                    ],
                ),
                ProjectEntry(
                    name="Vehicle Routing Service",
                    description="""
                    Implemented an algorithm for vehicle route optimization problems
                    for heterogeneous fleets. The problem statement is for a fleet of
                    vehicles to find the optimal route for each of them to pick up and
                    deliver cargo. The constraints supported were: Cargo capacity of
                    each vehicle; Time windows to pick-up and/or deliver;
                    specialization (only some type of vehicles could deliver some
                    cargo); support for multiple depots. Also different strategies
                    where developed to solve this problem, like finding the optimum
                    in terms of total distance to travel, or finding the optimum by
                    making each route length as equal as possible to the other ones.
                    """,
                    technologies=[
                        hyperlink(
                            "https://docs.oracle.com/javase/8/docs/api/",
                            "Java 8",
                        ),
                        hyperlink("https://spring.io/", "Spring Framework"),
                        hyperlink(
                            "https://en.wikipedia.org/wiki/Simulated_annealing",
                            "Simulated Annealing",
                        ),
                    ],
                ),
            ],
        ),
    ]

    return CVModel(
        personal_data=personal_data,
        education=education,
        work_experience=work_exp,
        projects=projects,
    )
